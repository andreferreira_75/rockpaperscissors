import java.io.Serializable;

public class Player {

    String name;
    GameElements hand;


    public Player(String name) {
        this.name = name;
    }

    public GameElements getRandomHand(){
        GameElements[] elements = GameElements.values();
        int index = (int) (Math.random() *3);
        hand = elements[index];
        return hand;
    }

    public GameElements getHand(){
        return this.hand = hand;
    }

}
